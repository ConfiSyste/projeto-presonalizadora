﻿namespace Personalizadora.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Mickey", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(44, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 26);
            this.label3.TabIndex = 17;
            this.label3.Text = "ImaginArt";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Mickey", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(681, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 26);
            this.label5.TabIndex = 19;
            this.label5.Text = "X";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.button15);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button13);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button12);
            this.panel1.Controls.Add(this.button11);
            this.panel1.Location = new System.Drawing.Point(12, 97);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(704, 259);
            this.panel1.TabIndex = 34;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(10, 79);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(225, 34);
            this.button11.TabIndex = 0;
            this.button11.Text = "Cadastrar produtos";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(239, 79);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(225, 34);
            this.button12.TabIndex = 22;
            this.button12.Text = "Consultar produtos";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(10, 41);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(225, 34);
            this.button1.TabIndex = 0;
            this.button1.Text = "Cadastrar funcionarios";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(239, 117);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(225, 34);
            this.button6.TabIndex = 24;
            this.button6.Text = "Consultar pedido";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(239, 41);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(225, 34);
            this.button4.TabIndex = 22;
            this.button4.Text = "Consultar funcionarios";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(239, 193);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(225, 34);
            this.button10.TabIndex = 28;
            this.button10.Text = "Consultar fornecedor";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(468, 41);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(225, 34);
            this.button3.TabIndex = 29;
            this.button3.Text = "Consultar folhas de pagamento";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(10, 193);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(225, 34);
            this.button9.TabIndex = 27;
            this.button9.Text = "Cadastrar fornecedor";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(239, 155);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(225, 34);
            this.button7.TabIndex = 30;
            this.button7.Text = "Consultar clientes";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(10, 155);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(225, 34);
            this.button8.TabIndex = 26;
            this.button8.Text = "Cadastras clientes";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(468, 193);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(225, 34);
            this.button13.TabIndex = 31;
            this.button13.Text = "Despesas";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(468, 155);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(225, 34);
            this.button5.TabIndex = 23;
            this.button5.Text = "Emitir folha de pagamento";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(468, 79);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(225, 34);
            this.button14.TabIndex = 32;
            this.button14.Text = "Fluxo de caixa";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(10, 117);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(225, 34);
            this.button2.TabIndex = 20;
            this.button2.Text = "Emitir pedido";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(468, 117);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(225, 34);
            this.button15.TabIndex = 33;
            this.button15.Text = "Estoque";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 374);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Italic);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmMenu";
            this.Text = "frmMenu";
            this.Load += new System.EventHandler(this.frmMenu_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
    }
}